This project demonstrates a problem with the default webpack configuration and workbox-webpack-plugin.

## Notes
- `npm run build:dev` works
- `npm run build:prod` doesn't work
- This is because of line 12-14 in `webpack.dev.js`:
```js
output: { // InjectManifest only works with this setting
    devtoolModuleFilenameTemplate: 'file:///[absolute-resource-path]'  // map to source with absolute file path not webpack:// protocol
}
```
## Error
```
PS C:\Users\USERNAME\dev\git\personal\webpack-problem> npm run build:prod

> webpack-problem@1.0.0 build:prod
> webpack --config webpack.prod.js

asset Test.bundle.js 618 bytes [compared for emit] (name: Test) 1 related asset
asset test-offline-worker.js 300 bytes [emitted] 1 related asset
./src/index.js 1 bytes [built] [code generated]

ERROR in Invalid URL: webpack://[name]/./src/test-offline-worker.js

webpack 5.31.2 compiled with 1 error in 104 ms
npm ERR! code 1
npm ERR! path C:\Users\USERNAME\dev\git\personal\webpack-problem
npm ERR! command failed
npm ERR! command C:\Windows\system32\cmd.exe /d /s /c webpack --config webpack.prod.js

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\Users\USERNAME\scoop\persist\nodejs\cache\_logs\2021-04-12T14_28_43_806Z-debug.log
```