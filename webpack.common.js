const path = require('path');
const { InjectManifest } = require('workbox-webpack-plugin');

module.exports = {
    entry: {
        Test: './src/index.js'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        library: "[name]", // this is line is making problems
        clean: true,
    },
    plugins: [
        new InjectManifest({
            swSrc: './src/test-offline-worker.js'
        }),
    ],
    watchOptions: {
        ignored: ['dist', '**/node_modules']
    },
    mode: 'development',
    devtool: 'source-map', // production source map
};